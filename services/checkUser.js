var allUsers = null;

exports.initUsers = function(req, res) {
    if (!allUsers) {
        req.getConnection(function (err, connection) {
            connection.query('SELECT * FROM user',function(err,rows)     {
                if(err)
                    console.log("Error Selecting : %s ", err);
                allUsers = rows;
            });
        });
    }
};

function isParametrExists(field, object, array, id) {
    var tempArr = [],
        j = 0,
        i;
    if (id) {
        for (i = 0; i < array.length; i++) {
            if (array[i].iduser.toString() !== id) {
                tempArr[j] = array[i];
                j++;
            }
        }
    } else {
        tempArr = array;
    }
    for (i = 0; i < tempArr.length; i++) {
        if(tempArr[i][field] === object[field]) {
            return true;
        }
    }
    return false;
}

function isCorrectValue(value) {
   return (value.replace(/[а-яА-Я]/g, '') === '');
}

function checkInfo(user, id){
    if (!allUsers || !allUsers.length) {
        return {
            status: true
        };
    }
    if (isParametrExists('passportNumber', user, allUsers, id) && isParametrExists('passportSeries', user, allUsers, id)) {
        return {
            status: false,
            message: 'Пользователь с такими паспортными данными уже существует'
        };
    }
    if (isParametrExists('identifier', user, allUsers, id)) {
        return {
            status: false,
            message: 'Пользователь с такими идентификационным номером уже существует'
        };
    }

    if(!isCorrectValue(user.surname)) {
        return {
            status: false,
            message: 'Некорректное значение для фамилии'
        }
    }

    if(!isCorrectValue(user.name)) {
        return {
            status: false,
            message: 'Некорректное значение для имени'
        }
    }

    if(!isCorrectValue(user.patronymic)) {
        return {
            status: false,
            message: 'Некорректное значение для отчество'
        }
    }

    return {
        status: true
    };
}

exports.isCorrectUser = function (req, res, flag) {
    var user, id;
    if (flag) {
        user = req.body.data;
        id = req.body.iduser;
        return checkInfo(user, id);
    } else {
        user = req.body;
        return checkInfo(user);
    }

};