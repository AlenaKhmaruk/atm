var checker = require('../services/checkUser');

exports.getAllUsers = function (req, res) {
    req.getConnection(function(err,connection){
        connection.query('SELECT * FROM user',function(err,rows)     {
            if(err)
                console.log("Error Selecting : %s ", err);
            res.send(JSON.stringify(rows));
        });

    });
};

exports.createUser = function (req, res) {
    var input = req.body,
        checkResult = checker.isCorrectUser(req, res);
    if (!checkResult.status) {
        res.send(checkResult);
        return;
    }
    req.getConnection(function (err, connection) {
        connection.query("INSERT INTO user set ? ", input, function(err, rows)
        {
            if (err) {
                console.log("Error inserting : %s ",err );
                res.send({
                    status: false,
                    message: err
                });
            } else {
                res.send({
                    status: true,
                    message: 'Пользователь успешно добавлен в базу данных'
                });
            }
        });
    });
};

exports.deleteUser = function (req, res) {
    var iduser = req.body.iduser;
    req.getConnection(function (err, connection) {
        connection.query("DELETE FROM user WHERE iduser = ? ", [iduser], function(err, rows)
        {
            if(err) {
                console.log("Error deleting : %s ",err );
            } else {
                res.send({
                    status: true,
                    message: 'Пользователь успешно удалён из базы данных'
                });
            }
        });

    });
};

exports.getUser = function (req, res) {
    var iduser = req.body.iduser;
    req.getConnection(function (err, connection) {
        connection.query("SELECT * FROM user WHERE iduser = ? ", [iduser], function(err, rows)
        {
            if(err)
                console.log("Error deleting : %s ",err );
            res.send(rows[0]);
        });

    });
};

exports.changeUser = function (req, res) {
    var iduser = req.body.iduser,
        data = req.body.data,
        checkResult = checker.isCorrectUser(req, res, true);
    if (!checkResult.status) {
        res.send(checkResult);
        return;
    }
    req.getConnection(function (err, connection) {
        connection.query("UPDATE user set ? WHERE iduser = ? ",[data, iduser], function(err, rows)
        {
            if(err) {
                console.log("Error deleting : %s ",err );
                res.send({
                    status: false,
                    message: err
                });
            } else {
                res.send({
                    status: true,
                    message: 'Пользователь успешно обновлён в базе данных'
                })
            }
        });

    });
};
