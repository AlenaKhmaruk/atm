var express = require('express'),
    mysql = require('mysql'),
    connection  = require('express-myconnection'),
    bodyParser = require('body-parser'),
    userInstance = require('./models/user'),
    checker = require('./services/checkUser');

var app = express();

app.use(express.static(__dirname + '/'));
app.use(bodyParser.json());

app.use(
    connection(mysql, {
        host: 'localhost',
        user: 'AlenaKhmaruk',
        password : 'lesenkavnebo104',
        port : 3306,
        database:'atmdb'
    }, 'request')
);

app.get(['/updateUser', '/updateUser/*', '/addUser', '/usersList', '/userInfo/*', '/userInfo', '/selectDeposit/*', '/createDeposit/*'],
    function(req, res){
        checker.initUsers(req, res);
        res.sendFile(__dirname + '/app/index.html');
    });

app.get('/getUsersList', function(req, res){
    userInstance.getAllUsers(req, res);
});

app.post('/createUser', function(req, res){
    userInstance.createUser(req, res);
});

app.post('/deleteUser', function(req, res){
    userInstance.deleteUser(req, res);
});

app.post('/getUserData', function(req, res){
    userInstance.getUser(req, res);
});

app.post('/changeUser', function(req, res) {
    userInstance.changeUser(req, res);
});

app.get('*', function(req, res) {
    res.redirect('/usersList');
});

app.listen(3000);

console.log('Server running on port 3000');
