var app = angular.module('atm', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function AppConfig($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/app/templates/main.tpl.html'
        })
        .when('/addUser', {
            template: '<custom-form></custom-form>'
        })
        .when('/updateUser/:id', {
            template: '<custom-form></custom-form>'
        })
        .when('/usersList', {
            template: '<users-list></users-list>'
        })
        .when('/userInfo/:id*', {
            template: '<user-info></user-info>'
        })
        .when('/selectDeposit/:id*', {
            template: '<banks-list></banks-list>'
        })
        .when('/createDeposit/:id*', {
            template: '<deposit-form></deposit-form>'
        })
        .otherwise(function(){
            redirect('/usersList')
        });
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
}]);
