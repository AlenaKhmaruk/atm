angular.module('atm')
    .controller('depositFormCtrl', ['$rootScope', '$scope', function($rootScope, $scope) {
        var selectedDeposit = $rootScope.selectedDeposit - 1;
        $scope.depositData = {
            currency: 'BYN',
            startDate: new Date(),
            sum: $rootScope.deposits[selectedDeposit].startDeposit
        };

        if (selectedDeposit || selectedDeposit === 0) {
            $scope.bankName = $rootScope.deposits[selectedDeposit].name;
        }
    }]);