angular.module('atm')
    .controller('userInfoCtrl', ['$scope', '$rootScope', 'dataTransferSrvc', '$routeParams',
        function($scope, $rootScope, dataTransferSrvc, $routeParams) {
            var id = $routeParams.id;
            var GENDERS = {'1': 'Мужской', '2': 'Женский'};
            var CITIES = {
                '1': 'Минск',
                '2': 'Гомель',
                '3': 'Гродно',
                '4': 'Витебск',
                '5': 'Брест',
                '6': 'Могилёв'
            };
            var MARITAL_STATUSES = {
                '1': 'Не женат/не замужем',
                '2': 'Женат/замужем'
            };
            var NATIONALITIES = {
                '1': 'Республика Беларусь',
                '2': 'Российская федерация',
                '3': 'США',
                '4': 'ФРГ'
            };
            var DISABILITIES = {
                '1': '1 ступень',
                '2': '2 ступень',
                '3': '3 ступень',
                '4': 'Отсутствует'
            };
            var PENSIONERS = {
                '1': 'да',
                '0': 'нет'
            };
            dataTransferSrvc.getUserData({iduser: id})
                .then(function success(response){
                    var tempData = response.data;
                    var birthday = new Date(tempData.birthday);
                    var dateIssuance = new Date(tempData.dateIssuance);
                    tempData.birthday = birthday.getDate() + '-' + (birthday.getMonth() + 1) + '-' + birthday.getFullYear();
                    tempData.dateIssuance = dateIssuance.getDate() + '-' + (dateIssuance.getMonth() + 1) + '-' + dateIssuance.getFullYear();
                    tempData.gender = GENDERS[tempData.gender];
                    tempData.city = CITIES[tempData.city];
                    tempData.maritalStatus = MARITAL_STATUSES[tempData.maritalStatus];
                    tempData.nationality = NATIONALITIES[tempData.nationality];
                    tempData.disability = DISABILITIES[tempData.disability];
                    tempData.pensioner = PENSIONERS[tempData.pensioner];
                    $scope.user = tempData;
                }, function error() {});
        }]);