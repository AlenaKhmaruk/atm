angular.module('atm')
    .controller('customFormCtrl', ['$rootScope', '$scope', '$timeout', '$location', '$routeParams', 'dataTransferSrvc',
        function($rootScope, $scope, $timeout, $location, $routeParams, dataTransferSrvc) {
            var defaultUser = {
                city: '1',
                gender: '1',
                maritalStatus: '1',
                nationality: '1',
                disability: '4',
                pensioner: false
            };
            var isNewUser = true;
            var id = $routeParams.id;

            dataTransferSrvc.getUserData({iduser: id})
                .then(function success(response) {
                    var data = response.data;
                    if (data) {
                        data.birthday = new Date(data.birthday);
                        data.dateIssuance = new Date(data.dateIssuance);
                        data.city = data.city.toString();
                        data.gender = data.gender.toString();
                        data.maritalStatus = data.maritalStatus.toString();
                        data.nationality = data.nationality.toString();
                        data.disability = data.disability.toString();
                        $scope.userData = data;
                        isNewUser = false;
                    } else {
                        $scope.userData = angular.copy(defaultUser);
                        isNewUser = true;
                    }
                }, function error(response) {});

            $scope.today = new Date();
            $scope.maxDate = new Date(angular.copy($scope.today)
                                        .setFullYear(new Date().getFullYear() - 18));

            $scope.$watch('userData.surname', function() {
                if ($scope.userData && $scope.userData.surname) {
                    $scope.userData.surname = $scope.userData.surname.replace(/[^а-яА-Я]/g, '');
                }
            });

            $scope.$watch('userData.name', function() {
                if ($scope.userData && $scope.userData.name) {
                    $scope.userData.name = $scope.userData.name.replace(/[^а-яА-Я]/g, '');
                }
            });

            $scope.$watch('userData.patronymic', function() {
                if ($scope.userData && $scope.userData.patronymic) {
                    $scope.userData.patronymic = $scope.userData.patronymic.replace(/[^а-яА-Я]/g, '');
                }
            });

            $scope.$watch('userData.passportSeries', function() {
                if ($scope.userData && $scope.userData.passportSeries) {
                    $scope.userData.passportSeries = $scope.userData.passportSeries.replace(/[^а-яА-Я]/g, '');
                }
            });

            $scope.$watch('userData.passportNumber', function() {
                if ($scope.userData && $scope.userData.passportNumber) {
                    $scope.userData.passportNumber = $scope.userData.passportNumber.replace(/[^0-9]/g, '');
                }
            });

            $scope.$watch('userData.identifier', function() {
                if ($scope.userData && $scope.userData.identifier) {
                    $scope.userData.identifier = $scope.userData.identifier.replace(/[^0-9a-zA-Z]/g, '');
                }
            });

            $scope.$watch('userData.housePhone', function() {
                if ($scope.userData && $scope.userData.housePhone){
                    $scope.userData.housePhone = $scope.userData.housePhone.replace(/[^0-9]/g, '');
                }
            });

            $scope.$watch('userData.mobilePhone', function() {
                if ($scope.userData && $scope.userData.mobilePhone) {
                    $scope.userData.mobilePhone = $scope.userData.mobilePhone.replace(/[^0-9]/g, '');
                }
            });

            $scope.$watch('userData.monthlyIncome', function() {
                if ($scope.userData && $scope.userData.monthlyIncome) {
                    $scope.userData.monthlyIncome = $scope.userData.monthlyIncome.toString().replace(/[^0-9]/g, '');
                }
            });

            $scope.sendData = function() {
                var data;
                if ($scope.userForm.$valid) {
                    data = angular.copy($scope.userData);
                    data.birthday = data.birthday.getFullYear() + '-' +
                                    (data.birthday.getMonth() + 1) + '-' +
                                    data.birthday.getDate();
                    data.dateIssuance = data.dateIssuance.getFullYear() + '-' +
                                        (data.dateIssuance.getMonth() + 1) + '-' +
                                        data.dateIssuance.getDate();
                    if (isNewUser) {
                        dataTransferSrvc.createUser(data)
                            .then(function(response) {
                                if (response.data.status) {
                                    console.log(response.data.message);
                                    $rootScope.consoleOutput += response.data.message + '<br>';
                                    $scope.userData = angular.copy(defaultUser);
                                    $location.path('/usersList');
                                } else {
                                    console.log(response.data.message);
                                    $rootScope.consoleOutput += response.data.message + '<br>';
                                }
                            });
                    } else {
                        dataTransferSrvc.changeUserData({iduser: id, data: data})
                            .then(function(response) {
                                if (response.data.status) {
                                    console.log(response.data.message);
                                    $rootScope.consoleOutput += response.data.message + '<br>';
                                    $location.path('/usersList');
                                } else {
                                    console.log(response.data.message);
                                    $rootScope.consoleOutput += response.data.message + '<br>';
                                }
                            });
                    }
                } else {
                    angular.element(document).find('form').addClass('invalid-form');
                }
            }
    }]);