angular.module('atm')
    .controller('mainCtrl', ['$rootScope', function($rootScope) {
        $rootScope.consoleOutput = '';
        var console;
        $rootScope.$watch('consoleOutput', function(){
            if (!console) {
                console = angular.element(document.querySelector('.database-console'))
            }
            if (console) {
                console[0].scrollTop = console[0].scrollHeight;
            }
        });
    }]);