angular.module('atm')
    .controller('usersListCtrl', ['$scope', '$rootScope', '$location', 'dataTransferSrvc',
        function($scope, $rootScope, $location, dataTransferSrvc) {
            dataTransferSrvc.getUsersList()
                .then(function mySuccess(response) {
                    $scope.users = response.data;
                }, function myError(response) {
                    console.log(response.statusText);
                });

            angular.element(document).find('users-list').bind('click', function(event){
                var classList = event.target.classList;
                var userID = event.target.parentElement.parentElement.getAttribute('data-user-id');
                if (classList.contains('fa-user')) {
                    $rootScope.$apply(function() {
                        $location.path('/userInfo/' + userID);
                    });
                } else if (classList.contains('fa-pencil')) {
                    $rootScope.$apply(function() {
                        $location.path('/updateUser/' + userID);
                    });
                } else if (classList.contains('fa-trash')) {
                    dataTransferSrvc.deleteUser({iduser: userID})
                        .then(function(response){
                            if (response.data.status) {
                                $rootScope.consoleOutput += response.data.message + '<br>';
                                event.target.parentElement.parentElement.remove();
                            }
                        }, function() {
                            console.log('error');
                        });
                } else if (classList.contains('fa-credit-card')) {
                    $rootScope.$apply(function() {
                        $location.path('/selectDeposit/' + userID);
                    });
                } else {
                    return 0;
                }
            });
        }]);