angular.module('atm')
    .controller('banksListCtrl', ['$scope', '$rootScope', '$location',
        function($scope, $rootScope, $location) {
            $rootScope.deposits = [
                {
                    id: 1,
                    name: 'Технобанк отзывной',
                    percent: 9,
                    capitalization: true,
                    startDeposit: 100,
                    increasing: 'ежемесячно',
                    time: 370
                },
                {
                    id: 2,
                    name: 'Технобанк безотзывной',
                    percent: 19,
                    capitalization: false,
                    startDeposit: 500,
                    increasing: 'ежегодно',
                    time: 370
                },
                {
                    id: 3,
                    name: 'Приорбанк отзывной',
                    percent: 7.5,
                    capitalization: true,
                    startDeposit: 100,
                    increasing: 'ежемесячно',
                    time: 540
                },
                {
                    id: 4,
                    name: 'Приорбанк безотзывной',
                    percent: 15,
                    capitalization: false,
                    startDeposit: 100,
                    increasing: 'ежегодно',
                    time: 540
                }
            ];

            $scope.getDepositForm = function(depositId) {
                var parsedUrl = $location.url().split('/');
                var userID = parsedUrl[parsedUrl.length - 1];
                $rootScope.selectedDeposit = depositId;
                $location.path('/createDeposit/' + userID);
            };
        }]);