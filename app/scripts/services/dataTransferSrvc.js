angular.module('atm')
    .service('dataTransferSrvc', ['$http', function($http) {
        this.createUser = function(data) {
            return $http({
                method : "POST",
                url : "/createUser",
                data: data
            });
        };

        this.getUsersList = function () {
            return $http({
                method : "GET",
                url : "/getUsersList"
            });
        };

        this.getUserData = function (data) {
            return $http({
                method : "POST",
                url : "/getUserData",
                data: data
            });
        };

        this.deleteUser = function(data) {
            return $http({
                method : "POST",
                url : "/deleteUser",
                data: data
            });
        };

        this.changeUserData = function(data) {
            return $http({
                method : "POST",
                url : "/changeUser",
                data: data
            });
        }
    }]);