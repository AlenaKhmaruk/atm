angular.module('atm')
    .directive('banksList', function(){
        return {
            restrict: 'E',
            templateUrl: '/app/templates/banksList.tpl.html',
            controller: 'banksListCtrl',
            controllerAs: 'banksListCtrl'
        }
    });