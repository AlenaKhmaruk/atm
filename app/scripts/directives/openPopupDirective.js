angular.module('atm')
    .directive('openPopup', function(){
        return {
            restrict: 'A',
            scope: true,
            controller: ['$scope', function($scope) {
                $scope.isOpen = false;

                $scope.openPopup = function ($event) {
                    $scope.isOpen = !$scope.isOpen;
                    $event.stopPropagation();
                }
            }],
            controllerAs: 'openPopupCtrl'
        }
    });