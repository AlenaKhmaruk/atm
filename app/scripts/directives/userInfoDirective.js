angular.module('atm')
    .directive('userInfo', function(){
        return {
            restrict: 'E',
            templateUrl: '/app/templates/userInfo.tpl.html',
            controller: 'userInfoCtrl',
            controllerAs: 'userInfoCtrl'
        }
    });