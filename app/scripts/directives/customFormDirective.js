angular.module('atm')
    .directive('customForm', function(){
        return {
            restrict: 'E',
            templateUrl: '/app/templates/customForm.tpl.html',
            controller: 'customFormCtrl',
            controllerAs: 'customFormCtrl'
        }
    });