angular.module('atm')
    .directive('usersList', function(){
        return {
            restrict: 'E',
            templateUrl: '/app/templates/usersList.tpl.html',
            controller: 'usersListCtrl',
            controllerAs: 'usersListCtrl'
        }
    });