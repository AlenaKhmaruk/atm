angular.module('atm')
    .directive('depositForm', function(){
        return {
            restrict: 'E',
            templateUrl: '/app/templates/depositForm.tpl.html',
            controller: 'depositFormCtrl',
            controllerAs: 'depositFormCtrl'
        }
    });