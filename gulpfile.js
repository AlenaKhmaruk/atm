var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload'),
    del = require('del');

gulp.task('default', ['clean'], function() {
    gulp.start('librariesStyles', 'scripts', 'styles');
});

gulp.task('librariesStyles', function() {
    return gulp.src('app/styles/reset.min.css')
        .pipe(concat('libraries.min.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('styles', function() {
    return sass('app/styles/*.scss', { style: 'expanded' })
        .pipe(concat('main.css'))
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('dist/css'))
        .pipe(rename({suffix: '.min'}))

        .pipe(gulp.dest('dist/css'));
});

gulp.task('clean', function() {
    return del('dist');
});

gulp.task('watch', function() {
    gulp.watch('app/styles/*.scss', ['styles']);
    gulp.watch('app/scripts/**/*.js', ['scripts']);
    livereload.listen();
    gulp.watch(['dist/**']).on('change', livereload.changed);
});
